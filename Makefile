# generates mock services
mock:
	go generate ./...

# generates swagger api docs
api-docs:
	swag init

# runs tests
run-tests:
	go clean -cache
	go test -v ./...

build:
	go build -o bin/main main.go

docker:
	docker buildx build -t remotejob/simple-go-prometheus:v0.0.1 --platform linux/amd64,linux/arm64 --push .

run:
	go run ./cmd/server