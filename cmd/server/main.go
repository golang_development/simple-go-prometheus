package main

import (
	"fmt"
	
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
)

var pingCounter = prometheus.NewCounter(
   prometheus.CounterOpts{
       Name: "ping_request_count",
       Help: "No of request handled by Ping handler",
   },
//    []string{"url"},
)

func ping(w http.ResponseWriter, req *http.Request) {
   pingCounter.Inc()
   fmt.Fprintf(w, "pong")
}

func main() {
   prometheus.MustRegister(pingCounter)

	   http.HandleFunc("/ping", ping)
	   http.Handle("/metrics", promhttp.Handler())
	   http.ListenAndServe(":8080", nil)
	}
// func main() {

// 	inFlightGauge := prometheus.NewGauge(prometheus.GaugeOpts{
// 		Name: "in_flight_requests",
// 		Help: "A gauge of requests currently being served by the wrapped handler.",
// 	})

// 	counter := prometheus.NewCounterVec(
// 		prometheus.CounterOpts{
// 			Name: "api_requests_total",
// 			Help: "A counter for requests to the wrapped handler.",
// 		},
// 		[]string{"code", "method"},
// 	)

// 	// duration is partitioned by the HTTP method and handler. It uses custom
// 	// buckets based on the expected request duration.
// 	duration := prometheus.NewHistogramVec(
// 		prometheus.HistogramOpts{
// 			Name:    "request_duration_seconds",
// 			Help:    "A histogram of latencies for requests.",
// 			Buckets: []float64{.25, .5, 1, 2.5, 5, 10},
// 		},
// 		[]string{"handler", "method"},
// 	)

// 	// responseSize has no labels, making it a zero-dimensional
// 	// ObserverVec.
// 	responseSize := prometheus.NewHistogramVec(
// 		prometheus.HistogramOpts{
// 			Name:    "response_size_bytes",
// 			Help:    "A histogram of response sizes for requests.",
// 			Buckets: []float64{200, 500, 900, 1500},
// 		},
// 		[]string{},
// 	)

// 	// Create the handlers that will be wrapped by the middleware.
// 	pushHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 		w.Write([]byte("Push"))
// 	})
// 	pullHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 		w.Write([]byte("Pull"))
// 	})

// 	// Register all of the metrics in the standard registry.
// 	prometheus.MustRegister(inFlightGauge, counter, duration, responseSize)

// 	// Instrument the handlers with all the metrics, injecting the "handler"
// 	// label by currying.
// 	pushChain := InstrumentHandlerInFlight(inFlightGauge,
// 		InstrumentHandlerDuration(duration.MustCurryWith(prometheus.Labels{"handler": "push"}),
// 			InstrumentHandlerCounter(counter,
// 				InstrumentHandlerResponseSize(responseSize, pushHandler),
// 			),
// 		),
// 	)
// 	pullChain := InstrumentHandlerInFlight(inFlightGauge,
// 		InstrumentHandlerDuration(duration.MustCurryWith(prometheus.Labels{"handler": "pull"}),
// 			InstrumentHandlerCounter(counter,
// 				InstrumentHandlerResponseSize(responseSize, pullHandler),
// 			),
// 		),
// 	)

// 	http.Handle("/metrics", Handler())
// 	http.Handle("/push", pushChain)
// 	http.Handle("/pull", pullChain)

// 	if err := http.ListenAndServe(":3000", nil); err != nil {
// 		log.Fatal(err)
// 	}

// }
